#!/usr/bin/awk -f

# This awk script is not designed to be called directly. It is a helper
# script for CI jobs which are wrappers around tests which have the
# following characteristics:
#   * examine exactly one revision of the repository
#   * can report one or more types of diagnostic, each one relating to a
#     specific line number of a specific file
#   * it may already report a non-zero number of diagnostics for the head
#     revision of the upstream repository
#   * it is desired that no additional such diagnostics should be introduced
#     as the result of merging future commits to the upstream repository
#   * the code may be modified to reduce the number of diagnostics over
#     time, but not as a prerequisite to enabling the CI job
#
# Instructions for use
#
# First, @include this file. Then define a function called `test`. This
# is passed an argument which is the SHA hash of the revision to assess.
# The job of this function is to spawn a process running the test, and
# post-process its output. The post-processing should populate members of
# the global array `record`. This array has four indices, in order:
#   * SHA hash, as passed as the function argument
#   * filename
#   * diagnostic name
#   * line number
#
# Remember that arrays in Awk are effectively hashes, so it doesn't matter
# that many of these indices are strings, rather than numbers. The value
# assigned can be an empty string, but if something more descriptive is
# used, it will be printed in the job log prefixed by the line number. If
# nothing else, it is suggested that the contents of the line could be used
# for this purpose.
#
# If it is not possible for `test` to perform its function, for example
# because it requires cross-compilation support which has not been
# implemented at the target revision, the function should return non-zero.
#
# A function `prerequisite` is provided as a service to any `test`
# functions. This executes a given bash command, echoing the command and
# any results if VERBOSE was set.
#
# Finally, write a BEGIN pattern that calls the function `analyse`. The
# first argument to this is the name of an environment variable containing
# a whitelist (suppression/enable specifier). The second argument is the
# string to match at the start of a line of a commit log: the remainder
# of that line is also treated as a whitelist specifier, possibly
# overriding the other whitelist specifiers. The third argument is used
# as a default whitelist specifier, and has the lowest priority. Within
# a whitelist, items further to the right have higher priority.
#
# Items within a whitelist are space-separated (unless wrapped with double
# quotes) and may have a leading '+' or '-' to indicate that they are a
# suppression or enable respectively. The default if neither is found is
# for it to act as a suppression. The rest of the item is used as a glob
# expression (with special characters '*', '?' or a '[]' range) which is
# matched against any of the following:
#  * <filename>
#  * <filename>:<line>
#  * <filename>:<diagnostic>
#  * <diagnostic>
#
# As a special case, items in whitelists in commit logs may use punctuation
# to make them flow more like natural language. For example:
#
#   Disable cppcheck for myfile.c, otherfile.c:123, thirdfile.c:456.
#
# Discussion of internal details
#
# The key challenge here is to establish what commits do correspond to
# the set of commits that have already been accepted to the upstream
# repository. Bear in mind here that CI jobs run on a clone of the _fork_
# repository, and most people's workflows typically do not involve keeping
# branches in their fork repositories up-to-date with the corresponding
# branch in the upstream repository. For example, it would be normal for
# the upstream head to be a few commits on from the fork head at the point
# where an engineer starts work on a feature branch based on the upstream
# head. When the feature branch is ready, it will then be pushed to the
# fork repository - but if you were only to compare the head from the fork
# repository to the feature branch, the diffs would include commits that
# had already been accepted upstream as well as the newly authored ones.
#
# To resolve this, we need the repository on the runner to fetch from the
# upstream repository as an additional remote. There are two problems here:
# 1) Access permissions. While CI creates a special token for accessing
#    the repository under test, persisting as long as the job does, this
#    only works for the fork repository. Fortunately, most upstream
#    repositories have public visibility, but for those that don't, we
#    need the @rool user to create a project access token (with
#    "read_repository" scope) for the upstream repository and assign it
#    as a CI variable "UPSTREAM_TOKEN" for each of its fork repositories.
#    We adapt the URL of the remote added depending upon whether this
#    variable is found in the environment or not.
# 2) Establishing the URL of the upstream repository. Following discussions
#    with a GitLab developer, it would appear that there is currently no
#    way to read the upstream fork relationship of a project from the
#    GitLab API, without the use of "read_api" token for that project.
#    To avoid the administrative headache of creating and assigning such
#    tokens for every fork on the server, I have created a workaround, which
#    is to fetch the HTML for the "Project overview" page for the fork
#    project (which apparently the CI token is good for, even for private
#    forks) and scrape the upstream URL from there. If a more official
#    way to achieve this is added in future, it's strongly recommended that
#    we adopt that!
#
# Once we have the upstream as a remote, we fetch its default head (which
# will usually correspond to either `master` or `main` depending upon its
# vintage). We then find the merge-base between this and the tip of the
# feature branch; the commits between the merge-base and the tip of the
# feature branch are the ones we deem to be significant. We try our best to
# ignore any diagnostics that were already reported at the merge-base.
#
# Because it's possible (if unlikely) that the commits in this set do not
# form a linear history, we actually specifically compare every commit
# against its first parent, discounting all others.
#
# Environment variables
#
# CI_COMMIT_SHA
#   Ref for which the pipeline is being run - typically the tip of a
#   feature branch. Set by CI system.
# CI_COMMIT_BRANCH
#   Branch name for which the pipeline is being run. Set by CI system.
# CI_PROJECT_PATH
#   Project namespace within the server - e.g. "RiscOS/Sources/Apps/Alarm".
#   Set by CI system.
# CI_REPOSITORY_URL
#   URL for cloning the (fork) repository. Typically includes username and
#   token for accessing private forks - e.g.
#   "https://gitlab-ci-token:[MASKED]@gitlab.riscosopen.org/rool/Alarm.git".
#   Set by CI system.
# CI_SERVER_HOST
#   Host of the GitLab instance, without protocol or port -
#   "gitlab.riscosopen.org". Set by CI system.
# CI_SERVER_URL
#   Base URL of the GitLab instance - "https://gitlab.riscosopen.org".
#   Set by CI system.
# MONTHLY_REVIEW
#   Set to 1 to enable an alternate mode of operation, only valid for
#   branch pipelines on upstream projects. In this case, we test all commits
#   on the given branch as far back as the earliest MR that was merged to it
#   within the last month. Typically, this environment variable is also used
#   by the calling script to suppress fewer diagnostics.
#   Set from web interface when defining a monthly pipeline schedule.
# UPSTREAM_TOKEN
#   Private access token required for accessing upstream project (if
#   private). Set from project settings in web interface.
# VERBOSE
#   Set to 1 to report more information about the diagnostics found.
#   Typically set manually when launching a pipeline.


function prerequisite(cmd) {
  if (verbose)
    print "\033[32m" cmd "\033[0m"
  cmd = "bash -c '" cmd "' 2>&1"
  while ((cmd | getline) > 0)
    if (verbose)
      print
  if (close(cmd))
    return 1
}

function globs_to_res(globs, regex_array, sense_array,         i) {
  patsplit(globs, regex_array, /([+-]?"(\\"|[^"])*")|[^ "]+/)
  for (i in regex_array)
  {
    sense_array[i] = "+"
    if (substr(regex_array[i], 1, 1) ~ /[+-]/)
    {
      sense_array[i] = substr(regex_array[i], 1, 1)
      regex_array[i] = substr(regex_array[i], 2)
    }
    if (substr(regex_array[i], 1, 1) == "\"")
      regex_array[i] = substr(regex_array[i], 2, length(regex_array[i]) - 2)
    gsub(/\\"/, "\"", regex_array[i])
    regex_array[i] = gensub(/([$()+.\\^{|}])/, "\\\\\\1", "g", regex_array[i])
    gsub(/\?/, ".", regex_array[i])
    gsub(/\*/, ".*", regex_array[i])
    regex_array[i] = "^" regex_array[i] "$"
  }
}

function lookup(regex_array, sense_array, diagnostic, file, line,
    # local variables follow
    file_line, file_diagnostic, i) {
  file_line = file ":" line
  file_diagnostic = file ":" diagnostic
  for (i = length(regex_array); i > 0; --i)
  {
    if (diagnostic ~ regex_array[i] ||
        file ~ regex_array[i] ||
        file_line ~ regex_array[i] ||
        file_diagnostic ~ regex_array[i])
      return sense_array[i] == "+"
  }
  return 0
}

function candidate(commit) {
  if (!(commit in parent)) # remove duplicates
  {
    sha[++i] = commit
    ("git rev-parse " sha[i] "^ 2> /dev/null") | getline parent[sha[i]]
    if (!(parent[sha[i]] ~ /\^$/ || parent[sha[i]] in parent))
    {
      discount[parent[sha[i]]] = test(parent[sha[i]])
      if (verbose)
        print ""
    }
    discount[sha[i]] = test(sha[i])
    if (verbose)
      print ""
  }
}

function analyse_branch(    commit, project, rc, upstream, upstream_token) {
  project = gensub(/[.]git$/, "", "g", ENVIRON["CI_REPOSITORY_URL"])
  if ((("curl \"" project "\" 2> /dev/null | grep forked_from_link") | getline) > 0)
  {
    upstream_token = ENVIRON["UPSTREAM_TOKEN"]
    if (upstream_token != "")
      upstream = "https://rool:" upstream_token "@" ENVIRON["CI_SERVER_HOST"]
    else
      upstream = ENVIRON["CI_SERVER_URL"]
    upstream = upstream gensub(/^.*href="([^"]+)".*$/, "\\1", "g") ".git"
  }
  else
    upstream = project ".git"
  system("git rev-parse --quiet --verify upstream/head > /dev/null && git branch --quiet -D upstream/head")
  system("git remote show | grep upstream > /dev/null && git remote remove upstream")
  rc = system("git remote add upstream " upstream)
  if (rc)
    exit(rc)
  rc = system("git fetch upstream HEAD:upstream/head 2> /dev/null")
  if (rc)
    exit(rc)
  commit = ENVIRON["CI_COMMIT_SHA"]
  ("git merge-base upstream/head " commit) | getline merge_base
  while ((("git rev-list --reverse " merge_base ".." commit "; echo " commit) | getline) > 0)
    candidate($0)
}

function analyse_monthly_mrs(    oldest_sha, python) {
  python = "python3 -"
  print "from datetime import datetime, timedelta, timezone" |& python
  print "from dateutil import parser" |& python
  print "from gitlab import gitlab" |& python
  print "from sys import stderr" |& python

  print "now = datetime.now(timezone.utc)" |& python
  print "cutoff = (now.replace(day=1) - timedelta(days=1)).replace(day=1,hour=0,minute=0,second=0,microsecond=0)" |& python
  print "oldest = now" |& python
  print "oldest_sha = ''" |& python
  print "gl=gitlab.Gitlab('" ENVIRON["CI_SERVER_URL"] "')" |& python
  print "p=gl.projects.get('" ENVIRON["CI_PROJECT_PATH"] "')" |& python
  print "print('The following MRs were merged to " ENVIRON["CI_COMMIT_BRANCH"] " in the last month:', file=stderr)" |& python
  print "for mr in p.mergerequests.list(get_all=True):" |& python
  # The test of merged_at below is due to a GitLab bug, supposedly fixed in
  # v15.0, see https://gitlab.com/gitlab-org/gitlab/-/issues/26911
  print "  if p.mergerequests.get(mr.iid).attributes['state'] == 'merged' and p.mergerequests.get(mr.iid).attributes['merged_at'] is not None and p.mergerequests.get(mr.iid).attributes['target_branch'] == '" ENVIRON["CI_COMMIT_BRANCH"] "':" |& python
  print "    merged_at = parser.isoparse(p.mergerequests.get(mr.iid).attributes['merged_at'])" |& python
  print "    age = (now - merged_at).days" |& python
  print "    if merged_at >= cutoff:" |& python
  print "      print(f'\033[32m!{mr.iid}\033[36m {mr.title}\033[0m {age} days ago', file=stderr)" |& python
  print "      if merged_at < oldest:" |& python
  print "        oldest = merged_at" |& python
  print "        for commit in p.mergerequests.get(mr.iid).commits(get_all=True):" |& python
  print "          oldest_sha = commit.id" |& python

  print "if oldest_sha == '':" |& python
  print "  print('None found', file=stderr)" |& python
  print "else:" |& python
  print "  print(oldest_sha)" |& python
  close(python, "to")
  python |& getline oldest_sha
  close(python)
  if (oldest_sha != "")
  {
    # The oldest commit in the oldest MR may still have been automatically
    # rewritten during the merge acceptance process, and thus not be in the
    # local clone (on the Runner machine). Therefore we need to explicitly
    # fetch it so that we can discover its parent, then work from there to
    # the end of the branch for which the pipeline is being run.
    system("git fetch origin " oldest_sha)
    while ((("git rev-list --reverse " oldest_sha "^.." ENVIRON["CI_COMMIT_BRANCH"]) | getline) > 0)
      candidate($0)
  }
}

function analyse(whitelist_var, log_introducer, default_whitelist,
    # local variables follow
    after_printed, before_printed, diagnostic, diagnostics,
    file, file_printed, files,
    i, line, result, sep, sha_printed,
    suppress, suppressed_after, suppressed_before,
    unsuppressed_after, unsuppressed_before,
    whitelist, whitelist_after,
    whitelist_re, whitelist_after_re,
    whitelist_sense, whitelist_after_sense) {
  result = 0
  whitelist = default_whitelist " " ENVIRON[whitelist_var]
  globs_to_res(whitelist, whitelist_re, whitelist_sense)
  verbose = ENVIRON["VERBOSE"]
  verbose = verbose != "" && verbose != 0
  if (ENVIRON["MONTHLY_REVIEW"] != "")
    analyse_monthly_mrs()
  else
    analyse_branch()

  for (i = 1; i <= length(sha); ++i)
  {
    if (discount[sha[i]] || discount[parent[sha[i]]])
    {
      ("git log -1 --pretty=format:%s " sha[i]) | getline summary
      print "\033[31mWarning: skipping \033[33m" substr(sha[i], 1, 8) " \033[36m" summary
      printf("\033[31mbecause diagnostics for %s%s%s are not available\033[0m\n", discount[sha[i]] ? "commit" : "", discount[sha[i]] && discount[parent[sha[i]]] ? " and " : "", discount[parent[sha[i]]] ? "parent commit" : "")
      continue;
    }
    whitelist_after = whitelist
    while ((("git log -1 --pretty=format:%b " sha[i]) | getline body) > 0)
      if (match(body, "^ *" log_introducer " +"))
        whitelist_after = whitelist_after " " gensub(/, |\.$/, " ", "g", substr(body, RSTART+RLENGTH))
    delete whitelist_after_re
    globs_to_res(whitelist_after, whitelist_after_re, whitelist_after_sense)
    sha_printed = 0
    PROCINFO["sorted_in"] = "@ind_str_asc"
    delete files
    if (parent[sha[i]] in record)
      for (file in record[parent[sha[i]]])
        files[file]
    if (sha[i] in record)
      for (file in record[sha[i]])
        files[file]
    for (file in files)
    {
      file_printed = 0
      PROCINFO["sorted_in"] = "@ind_str_asc"
      delete diagnostics
      if (parent[sha[i]] in record && file in record[parent[sha[i]]])
        for (diagnostic in record[parent[sha[i]]][file])
          diagnostics[diagnostic]
      if (sha[i] in record && file in record[sha[i]])
        for (diagnostic in record[sha[i]][file])
          diagnostics[diagnostic]
      for (diagnostic in diagnostics)
      {
        unsuppressed_before = 0
        suppressed_before = 0
        unsuppressed_after = 0
        suppressed_after = 0
        if (parent[sha[i]] in record && file in record[parent[sha[i]]] && diagnostic in record[parent[sha[i]]][file])
          for (line in record[parent[sha[i]]][file][diagnostic])
          {
            if (lookup(whitelist_re, whitelist_sense, diagnostic, file, line))
              ++suppressed_before
            else
              ++unsuppressed_before
          }
        if (sha[i] in record && file in record[sha[i]] && diagnostic in record[sha[i]][file])
          for (line in record[sha[i]][file][diagnostic])
          {
            if (lookup(whitelist_after_re, whitelist_after_sense, diagnostic, file, line))
              ++suppressed_after
            else
              ++unsuppressed_after
          }
        if (unsuppressed_after > unsuppressed_before)
          ++result
        if (unsuppressed_after > unsuppressed_before ||
            (unsuppressed_after + suppressed_after) > (unsuppressed_before + suppressed_before) ||
            verbose)
        {
          if (!sha_printed)
          {
            ("git log -1 --pretty=format:%s " sha[i]) | getline summary
            print "Commit \033[33m" substr(sha[i], 1, 8) " \033[36m" summary "\033[0m"
            sha_printed = 1
          }
          if (!file_printed)
          {
            print "  in file \033[32m" file "\033[0m"
            file_printed = 1
          }
          PROCINFO["sorted_in"] = "@ind_num_asc"
          if (unsuppressed_after == unsuppressed_before && suppressed_after == suppressed_before)
            print "    no net change of \033[31m" diagnostic "\033[0m"
          else
          {
            printf("    ");
            if (unsuppressed_after > unsuppressed_before)
              printf("%sadded " unsuppressed_after - unsuppressed_before, unsuppressed_before > 0 ? "net-" : "")
            else if (unsuppressed_after < unsuppressed_before)
              printf("%sremoved " unsuppressed_before - unsuppressed_after, unsuppressed_after > 0 ? "net-" : "")
            if (unsuppressed_after != unsuppressed_before)
              sep = " and "
            else
              sep = ""
            if (suppressed_after > suppressed_before)
              printf(sep "%sadded " suppressed_after - suppressed_before " suppressed", suppressed_before > 0 ? "net-" : "")
            else if (suppressed_after < suppressed_before)
              printf(sep "%sremoved " suppressed_before - suppressed_after " suppressed", suppressed_after > 0 ? "net-" : "")
            print " \033[31m" diagnostic "\033[0m"
          }
          if (unsuppressed_before > 0 || suppressed_before > 0)
          {
            before_printed = unsuppressed_after == 0 && suppressed_after == 0
            for (line in record[parent[sha[i]]][file][diagnostic])
            {
              if (record[parent[sha[i]]][file][diagnostic][line] != "")
              {
                if (!before_printed)
                {
                  print "    before"
                  before_printed = 1
                }
                suppress = lookup(whitelist_re, whitelist_sense, diagnostic, file, line)
                printf("    \033[35m%6u: \033[0%sm%s\033[0m\n", line, suppress ? ";9" : "", record[parent[sha[i]]][file][diagnostic][line])
              }
            }
          }
          if (unsuppressed_after > 0 || suppressed_after > 0)
          {
            after_printed = unsuppressed_before == 0 && suppressed_before == 0
            for (line in record[sha[i]][file][diagnostic])
            {
              if (record[sha[i]][file][diagnostic][line] != "")
              {
                if (!after_printed)
                {
                  print "    after"
                  after_printed = 1
                }
                suppress = lookup(whitelist_after_re, whitelist_after_sense, diagnostic, file, line)
                printf("    \033[35m%6u: \033[0%sm%s\033[0m\n", line, suppress ? ";9" : "", record[sha[i]][file][diagnostic][line])
              }
            }
          }
        }
      }
    }
  }
  if (result == 0)
    print "\033[32mTest passed OK\033[0m"
  else
  {
    printf("\033[31m" result " issue%s found\033[0m\n", result > 1 ? "s" : "")
    if (ENVIRON["MONTHLY_REVIEW"] == "")
      print "Please correct (or adjust the commit log or .gitlab-ci.yml) then resubmit"
    else
      exit(0)
  }
  exit(result)
}
