# CI_Source

This project contains a script that autogenerates the YAML configuration
files for GitLab CI in most of the other projects on this server.

It can be executed manually, but most often it is expected to operate as
part of this project's own CD job.
